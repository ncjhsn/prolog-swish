parent(antonio,joaozinho).
parent(maria,joaozinho).
parent(antonio,luis).
parent(maria,luis).
parent(antonio, cristina).
parent(maria, cristina).
parent(carlos, antonio).
parent(fernanda, antonio).
parent(pedro, maria).
parent(mariana, maria).
parent(jose, mariana).
parent(sandra, mariana).
parent(carlos, alberto).
parent(fernanda, alberto).
parent(carlos, marcia).
parent(fernanda, marcia).

gender(joaozinho, male).
gender(luis, male).
gender(antonio, male).
gender(alberto, male).
gender(maria, fem).
gender(marcia, fem).
gender(fernanda, fem).
gender(sandra, fem).
gender(mariana, fem).
gender(jose, male).
gender(pedro, male).
gender(carlos, male).

father(X, Y):- parent(X, Y), gender(X, male).
mother(X, Y):- parent(X, Y), gender(X, fem).
brother(X, Y):- parent(Z, X), parent(Z, Y), X\=Y, gender(X, male). 
sister(X, Y):- parent(Z, X), parent(Z, Y), X\=Y, gender(X, fem).

uncle(X, Y):- parent(Z, Y), brother(X, Z), X\=Z.
aunt(X, Y):- parent(Z, Y), sister(X, Z), X\=Z.

grandFather(X, Y):- parent(Z, Y), parent(X, Z), gender(X, male).
grandMother(X, Y):- parent(Z, Y), parent(X, Z), gender(X, fem).

greatGrandFather(X, Y):- grandMother(Z, Y), parent(X, Z), gender(X, male).
greatGrandMother(X, Y):- grandMother(Z, Y), parent(X, Z), gender(X, fem).

ancestor(X, Y):- parent(X, Y).
ancestor(X, Y):- parent(Z, Y), ancestor(X, Z).